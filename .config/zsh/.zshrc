export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="robbyrussell"
CASE_SENSITIVE="true"
HYPHEN_INSENSITIVE="true"
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
HIST_STAMPS="yyyy-mm-dd"
zstyle ':omz:update' mode auto
zstyle ':omz:update' frequency 13
setopt autocd extendedglob
bindkey -v
plugins=(git man rand-quote sudo timer)
source $ZSH/oh-my-zsh.sh

[ -f ~/.config/zsh-snap/znap.zsh ] ||
    git clone --depth 1 -- \
        https://github.com/marlonrichert/zsh-snap.git ~/.config/zsh-snap

source ~/.config/zsh-snap/znap.zsh

if [ -d "$HOME/.bin" ]; then
  PATH="$HOME/.bin/:$PATH"
fi

if [ -d "$HOME/.local/bin" ]; then
  PATH="$HOME/.local/bin:$PATH"
fi

export EDITOR='nvim'
export IDE='emacsclient -c'

alias vim="$EDITOR"
alias cat="bat"
alias doom='~/.emacs.d/bin/doom'
alias kdiff="kitty +kitten diff"

alias nvimconfig="$IDE ~/.config/nvim/README.org &"
alias zshconfig="$IDE ~/.config/zsh/README.org &"
alias doomconfig="$IDE ~/.doom.d/config.org &"

if [ "$TERM" = 'xterm-kitty' ]; then
  alias termconfig="$IDE ~/.config/kitty/README.org &"
elif [ "$TERM" = 'xterm-alacritty' ]; then
  alias termconfig="$IDE ~/.config/alacritty/README.org &"
fi

if [ ! -z $(command -v nix) ]; then
  alias nix-conf="(cd /etc/nixos && sudo $EDITOR configuration.nix)"
  alias rebuild='sudo nixos-rebuild switch'
fi

if [ ! -z $(command -v exa) ]; then
  alias ls='exa -al --color=always --group-directories-first'
  alias la='exa -a  --color=always --group-directories-first'
  alias ll='exa -l  --color=always --group-directories-first'
  alias lt='exa -aT --color=always --group-directories-first'
  alias l.='exa -a | egrep "^\."'
fi

alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
alias rmf="rm -rf"

if [ ! -z $(command -v git) ]; then
  alias config="git --git-dir=$HOME/.dot/ --work-tree=$HOME"
  alias gcl='git clone'
  alias gclr='git clone --recurse-submodules "$@" && cd "$(basename $_.git)"'
  alias gcm='git checkout master'
  alias gcd='git checkout develop'
  alias glog="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --stat"
  alias gres='git reset --hard && git pull'
  alias yolo='git commit -m "$(curl -s http://whatthecommit.com/index.txt)"'
fi

if [ ! -z $(command -v deadbeef) ]; then
  alias playwav='deadbeef *.wav'
  alias playogg='deadbeef *.ogg'
  alias playmp3='deadbeef *.mp3'
fi

if [ ! -z $(command -v mpv) ]; then
  alias playavi='mpv *.avi'
  alias playmov='mpv *.mov'
  alias playmp4='mpv *.mp4'
fi

if [ ! -z $(command -v yt-dlp) ]; then
  alias yta-aac="yt-dlp --extract-audio --audio-format aac "
  alias yta-best="yt-dlp --extract-audio --audio-format best "
  alias yta-flac="yt-dlp --extract-audio --audio-format flac "
  alias yta-m4a="yt-dlp --extract-audio --audio-format m4a "
  alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
  alias yta-opus="yt-dlp --extract-audio --audio-format opus "
  alias yta-vorbis="yt-dlp --extract-audio --audio-format vorbis "
  alias yta-wav="yt-dlp --extract-audio --audio-format wav "
  alias ytv-best="yt-dlp -f bestvideo+bestaudio "
fi

alias hsi='history | grep -i'
alias reload='omz reload'
alias remacs='doom sync && systemctl restart emacs --user'

eval "$(direnv hook zsh)"

export PF_INFO="ascii title os kernel shell wm uptime pkgs memory palette"
export PF_COL1=4
export PF_COL2=9
export PF_COL3=1

clear
command -v pfetch > /dev/null && pfetch
