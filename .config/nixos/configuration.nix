# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [
      ./hardware-configuration.nix # !HAS TO BE GENERATED!
      ./packages.nix
    ];

  ### Boot Options
  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot";
    };
  };

  ## Nix OS
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    settings = {
      auto-optimise-store = true;
      substituters = [
        "https://nix-community.cachix.org"
        "https://cache.nixos.org"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };
  };

  ### Networking
  networking.hostName = "doom";
  networking.networkmanager.enable = true;

  time.timeZone = "Europe/Berlin";

  ### Language & Fonts
  i18n = {
    defaultLocale = "en_US.UTF-8";
    inputMethod.enabled = "fcitx5";
    inputMethod.fcitx5.addons = [ pkgs.fcitx5-mozc ];
  };
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
  fonts.fonts = builtins.attrValues {
    inherit (pkgs)
    mononoki fira-code nerdfonts julia-mono ibm-plex
    source-han-serif source-han-sans source-han-mono
    noto-fonts noto-fonts-extra noto-fonts-emoji
    noto-fonts-cjk-sans noto-fonts-cjk-serif
    merriweather alegreya liberation_ttf;
  };

  nixpkgs.overlays = [
    (import (builtins.fetchTarball "https://github.com/nix-community/emacs-overlay/archive/master.tar.gz"))
    (import ./qtile.nix { inherit pkgs lib; })
  ]; 

  ### Desktop Environment
  services = {
    # Enable X11
    xserver = {
      enable = true;
      displayManager = {
        sddm.enable = true;
        defaultSession = "none+qtile";
      };
      windowManager = {
        icewm.enable = true;
        # Built in Overlay
        qtile.enable = true;
        xmonad = {
          enable = true;
          enableContribAndExtras = true;
          extraPackages = hpkgs: [
            hpkgs.xmonad
            hpkgs.xmonad-contrib
            hpkgs.xmonad-extras
          ];
        };
      };
      desktopManager.plasma5.enable = true;

      # Touchpad Support
      libinput.enable = true;
      layout = "us";
    };

    # Enable CUPS Services
    printing.enable = true;

    # Enable the SSH Daemon
    openssh.enable = true;

    # Gnome Keyring
    gnome.gnome-keyring.enable = true;

    # Enable the MPD
    mpd = {
      enable = true;
      extraConfig = ''
        audio_output {
          type "pulse"
          name "PulseAudio Adapter"
          server "127.0.0.1"
        }
      '';
    };

    # Enable Emacs Native Comp in Server Mode
    emacs = {
      enable = true;
      package = pkgs.emacsGitNativeComp;
    };
  };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    extraConfig = "load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1";
  };

  xdg.mime.enable = true;

  users.users.doom = {
    isNormalUser = true;
    extraGroups = [ "wheel" "network" ];
    packages = builtins.attrValues {
      inherit (pkgs)
      element-desktop mailspring
      tdesktop telegram-cli
      tor-browser-bundle-bin;
    };
  };

  programs = {
   gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };

    neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;

      configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [
            nvim-treesitter
          ];
        };
      };
    };

    nm-applet.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}

