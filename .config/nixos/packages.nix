{ pkgs, unstable, ... }:

let
  general_pkgs = builtins.attrValues {
    inherit (pkgs)
    alacritty brave deadbeef
    feh gimp inkscape keepassxc
    kitty mpv nitrogen picom
    rofi sweet zathura zotero
    networkmanagerapplet volctl
    eww i3lock-color papirus-icon-theme
    audacious pavucontrol;
  };
  xfce_pkgs = builtins.attrValues {
    inherit (pkgs.xfce)
    thunar thunar-volman
    thunar-archive-plugin
    thunar-media-tags-plugin;
  };
  develop_pkgs = builtins.attrValues {
    inherit (pkgs)
    gcc clang clang-tools cppcheck
    include-what-you-use glslang
    valgrind yasm shellcheck shfmt
    julia-bin texlab nodejs
    opam ocamlformat dune_3
    python39Full black pipenv
    sumneko-lua-language-server
    luajit html-tidy;
  };
  cli_pkgs = builtins.attrValues {
    inherit (pkgs)
    cmigemo curl oh-my-zsh wget jq
    zsh docker terraform terraform-ls
    heroku coreutils gnumake cmake
    git exa fd direnv bottom dos2unix
    editorconfig-core-c gvfs ntfs3g
    ntp nmap yt-dlp tokei onefetch
    neofetch nixfmt pandoc poppler
    ripgrep rtags sqlite neovim unzip
    libsecret cachix mu isync xclip 
    scrot graphviz gnuplot xdotool 
    delta bat imagemagick pywal killall
    pfetch kitty-themes efibootmgr
    refind pipes pulsemixer brightnessctl
    playerctl mpc-cli figlet ffmpeg
    nix-prefetch-scripts iw wirelesstools
    hadolint gitlint;
  };
  ocaml_pkgs = builtins.attrValues {
    inherit (pkgs.ocamlPackages)
    utop ocp-indent merlin;
  };
  pip_pkgs = builtins.attrValues {
    inherit (pkgs.python39Packages)
    pip nose pytest setuptools pyflakes isort;
  };
  node_pkgs = builtins.attrValues {
    inherit (pkgs.nodePackages)
    stylelint js-beautify;
  };
  xorg_pkgs = builtins.attrValues {
    inherit (pkgs.xorg)
    xwininfo xdpyinfo;
  };
in
{
  environment.systemPackages =
    general_pkgs ++ xfce_pkgs ++
    develop_pkgs ++ cli_pkgs ++
    ocaml_pkgs ++ pip_pkgs ++
    node_pkgs ++ xorg_pkgs ++ [
      pkgs.texlive.combined.scheme-full
      (pkgs.aspellWithDicts (dicts: [
        pkgs.aspellDicts.en
        pkgs.aspellDicts.de
        pkgs.aspellDicts.en-science
        pkgs.aspellDicts.en-computers
      ]))
      (pkgs.vimPlugins.nvim-treesitter.withPlugins (plugins: with pkgs.tree-sitter-grammars; [
        # Low-Level Languages
        tree-sitter-c
        tree-sitter-cpp
        tree-sitter-glsl
        tree-sitter-cuda
        tree-sitter-llvm
        tree-sitter-make
        tree-sitter-cmake
        tree-sitter-rust
        tree-sitter-julia
        tree-sitter-verilog
        # General Purpose
        tree-sitter-bash
        tree-sitter-lua
        tree-sitter-elisp
        tree-sitter-python
        tree-sitter-java
        tree-sitter-regex
        # Doc
        tree-sitter-latex
        tree-sitter-bibtex
        tree-sitter-markdown
        tree-sitter-org-nvim
        # Functional
        tree-sitter-ocaml
        tree-sitter-ocaml-interface
        tree-sitter-haskell
        # Web
        tree-sitter-vue
        tree-sitter-svelte
        tree-sitter-tsx
        tree-sitter-typescript
        tree-sitter-javascript
        tree-sitter-css
        tree-sitter-scss
        tree-sitter-html
        tree-sitter-prisma
        tree-sitter-graphql
        # Configuration
        tree-sitter-nix
        tree-sitter-hcl
        tree-sitter-dot
        tree-sitter-toml
        tree-sitter-yaml
        tree-sitter-json
        tree-sitter-json5
        tree-sitter-dockerfile
      ]))
    ];
}
