{pkgs, lib, ...}:
let
  overlay = self: super: { qtile = super.qtile.unwrapped.override (old: rec {
    src = super.fetchFromGitHub {
      owner = "qtile";
      repo = "qtile";
      rev = ''6620786f801f56fc61716514139044108eaef743'';
      sha256 = ''1x33c9fcg4iz8857zah19b9gsazjxm5qldgwv2qswryfqxibjqyh'';
    };

    qtile-extras = pkgs.python3Packages.buildPythonPackage {
      pname = "qtile-extras";
      version = "0.1.0";
      src = super.fetchgit {
        url = "https://github.com/elparaguayo/qtile-extras";
        rev = ''e418f310267e56ac1b9771721b034a7e973d514b'';
        sha256 = ''1c1gnlzpbcl1py9rsbsl36sanjhfhxviy85zwwnnms8fmzdxp6y5'';
        leaveDotGit = true;
      };
      doCheck = false; # don't run tests as it can't find libqtile
      nativeBuildInputs = with pkgs; [ git ];
      buildInputs = with pkgs.python3Packages; [ setuptools_scm ];
      meta = with super.lib; {
        homepage = "https://github.com/elparaguayo/qtile-extras";
        license = licenses.mit;
        description = "Extras for Qtile";
        platforms = platforms.linux;
      };
    };

    propagatedBuildInputs = (old.propagatedBuildInputs or []) ++ (with pkgs; [
      libinput libxkbcommon pulseaudio wayland wlroots qtile-extras
    ]) ++ (with pkgs.python3Packages; [
      dbus-next       # (Keyboard & Periphery)     DBus library with asyncio support
      psutil          # (Sensors, Memory & Power)  System Utilization Information Interface
      keyring         # (Keyring)                  Python & Keyring
      xcffib          # (qtile-extras)             XCB Bindings
      pyxdg           # (Launch Bar)               FreeDesktop.org Standards
      mypy            # (Check Config)             Static Typing for Python
    ]);
  });
};
in overlay
