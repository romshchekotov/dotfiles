# Doom's QTile Configuration
A lot of this is heavily Work in Progress, especially all the fancy
widgets. Stay tuned to see how it turns out.

## TODO-List
- Design Brand Icon "Douuma"
- Patch up Wlan Widget on NixOS
- Integrate more Scripts into the Workflow
- Wire up all 'special' Menu-Scripts appropriately and bundle them with the config
