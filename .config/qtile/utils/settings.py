# Catppuccin colors
colors = [
    ["#DDB6F2", "#DDB6F2"],  #  0 mauve
    ["#F5C2E7", "#F5C2E7"],  #  1 pink
    ["#E8A2AF", "#E8A2AF"],  #  2 maroon
    ["#F28FAD", "#F28FAD"],  #  3 red
    ["#F8BD96", "#F8BD96"],  #  4 peach
    ["#FAE3B0", "#FAE3B0"],  #  5 yellow
    ["#ABE9B3", "#ABE9B3"],  #  6 green
    ["#B5E8E0", "#B5E8E0"],  #  7 teal
    ["#96CDFB", "#96CDFB"],  #  8 blue
    ["#89DCEB", "#89DCEB"],  #  9 sky
    ["#161320", "#161320"],  # 10 black 0
    ["#1A1826", "#1A1826"],  # 11 black 1
    ["#1E1E2E", "#1E1E2E"],  # 12 black 2
    ["#302D41", "#302D41"],  # 13 black 3
    ["#575268", "#575268"],  # 14 black 4
    ["#6E6C7E", "#6E6C7E"],  # 15 gray 0
    ["#988BA2", "#988BA2"],  # 16 gray 1
    ["#C3BAC6", "#C3BAC6"],  # 17 gray 2
    ["#D9E0EE", "#D9E0EE"],  # 18 white
    ["#C9CBFF", "#C9CBFF"],  # 19 lavender
    ["#F5E0DC", "#F5E0DC"],  # 20 rosewater
]

# Without names
workspace_names = [
    "",
    "",
    "",
    "",
    "",
    "切",
    "龎",
]

# Wallpapers
#wallpaper_main = "~/.config/qtile/wallpapers/wires.jpg"
wallpaper_main = "~/.config/qtile/wallpapers/coast-view.jpg"
wallpaper_sec = "~/.config/qtile/wallpapers/coast-view.jpg"

# Misc.
ethernet = "enp0s20f0u2u4"
wifi = "wlp0s20f3"
num_monitors = 2
has_battery = True
