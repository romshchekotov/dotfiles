from libqtile import bar, qtile, lazy

from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration

from utils.settings import colors, has_battery, wifi, workspace_names

import os

home = os.path.expanduser("~")

group_box_settings = {
    "active": colors[0],
    "block_highlight_text_color": colors[0],
    "this_current_screen_border": colors[0],
    "this_screen_border": colors[0],
    "urgent_border": colors[3],
    "background": colors[12],  # background is [10-12]
    "other_current_screen_border": colors[12],
    "other_screen_border": colors[12],
    "highlight_color": colors[13],
    "inactive": colors[14],
    "foreground": colors[18],
    "borderwidth": 2,
    "disable_drag": True,
    "fontsize": 14,
    "highlight_method": "line",
    "padding_x": 10,
    "padding_y": 16,
    "rounded": False,
}

# functions for callbacks
def open_launcher():
    qtile.cmd_spawn("rofi -show drun -theme ~/.config/rofi/launcher.rasi")

def open_powermenu():
    qtile.cmd_spawn(home + "/.local/bin/power")

def toggle_maximize():
    lazy.window.toggle_maximize()

def separator():
    return widget.Sep(
        # foreground=colors[18],
        foreground=colors[12],
        padding=4,
        linewidth=3,
    )


def separator_sm():
    return widget.Sep(
        # foreground=colors[18],
        foreground=colors[12],
        padding=1,
        linewidth=1,
        size_percent=55,
    )


# widget decorations
base_decor = {
    "colour": colors[13],
    "filled": True,
    "padding_y": 4,
    "line_width": 0,
}


def _full_decor(color):
    return [
        RectDecoration(
            colour=color,
            radius=4,
            filled=True,
            padding_y=4,
        )
    ]


def _left_decor(color, padding_x=None, padding_y=4):
    return [
        RectDecoration(
            colour=color,
            radius=4,
            filled=True,
            padding_x=padding_x,
            padding_y=padding_y,
        )
    ]


def _right_decor(color):
    return [
        RectDecoration(
            colour=colors[13],
            radius=4,
            filled=True,
            padding_y=4,
            padding_x=0,
        )
    ]


# TODO: Main Icon
w_hk = widget.Image(
    background=colors[0],
    margin_x=14,
    margin_y=3,
    mouse_callbacks={"Button1": open_launcher},
    filename="~/.config/qtile/icons/douma.png",
)

# left icon
w_sys_icon = widget.TextBox(
    text="",
    font="FiraMono Nerd Font",
    fontsize=22,
    foreground="#000000",
    # foreground=colors[2],
    background=colors[0],
    padding=16,
    mouse_callbacks={"Button1": open_launcher},
)

# workspace groups
w_groupbox_1 = widget.GroupBox(  # GEN
    font="FiraMono Nerd Font",
    visible_groups=[workspace_names[0]],
    **group_box_settings,
)

w_groupbox_2 = widget.GroupBox(  # DEV, SYS
    font="FiraMono Nerd Font",
    visible_groups=[workspace_names[1], workspace_names[2]],
    **group_box_settings,
)

w_groupbox_3 = widget.GroupBox(  # LAB, MUS
    font="FiraMono Nerd Font",
    visible_groups=[workspace_names[3], workspace_names[4]],
    **group_box_settings,
)

w_groupbox_4 = widget.GroupBox(  # SOCIAL, READ
    font="FiraMono Nerd Font",
    visible_groups=[workspace_names[5], workspace_names[6]],
    **group_box_settings,
)


def gen_groupbox():
    return (
        widget.GroupBox(  # GEN
            font="FiraMono Nerd Font",
            visible_groups=[workspace_names[0]],
            **group_box_settings,
        ),
        widget.GroupBox(  # DEV, SYS
            font="FiraMono Nerd Font",
            visible_groups=[workspace_names[1], workspace_names[2]],
            **group_box_settings,
        ),
        widget.GroupBox(  # LAB, MUS
            font="FiraMono Nerd Font",
            visible_groups=[workspace_names[3], workspace_names[4]],
            **group_box_settings,
        ),
        widget.GroupBox(  # SOCIAL, READ
            font="FiraMono Nerd Font",
            visible_groups=[workspace_names[5], workspace_names[6]],
            **group_box_settings,
        ),
    )


# spacers
def gen_spacer():
    return widget.Spacer()


# window name
w_window_name_icon = widget.TextBox(
    text=" ",
    foreground="#ffffff",
    font="FiraMono Nerd Font",
)

w_window_name = widget.WindowName(
    foreground="#ffffff",
    width=bar.CALCULATED,
    empty_group_string="Desktop",
    max_chars=40,
    mouse_callbacks={"Button1": toggle_maximize},
)

# systray
w_systray = widget.Systray(
    padding=5,
)

# current layout
def gen_current_layout():
    color = colors[5]
    return (
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            padding=3,
            scale=0.65,
            use_mask=True,
            foreground=colors[12],
            decorations=_left_decor(color),
        ),
        separator_sm(),
        widget.CurrentLayout(
            foreground=color,
            padding=8,
            decorations=_right_decor(color),
        ),
        separator(),
    )


# battery
w_battery = (
    (
        widget.Battery(
            format="{char}",
            charge_char="",
            discharge_char="",
            full_char="",
            unknown_char="",
            empty_char="",
            show_short_text=False,
            foreground=colors[10],
            fontsize=18,
            padding=8,
            decorations=_left_decor(colors[1]),
        ),
        separator_sm(),
        widget.Battery(
            format="{percent:2.0%}",
            show_short_text=False,
            foreground=colors[1],
            padding=8,
            decorations=_right_decor(colors[5]),
        ),
        separator(),
    )
    if has_battery
    else ()
)

# volume
w_volume_icon = widget.TextBox(
    text="墳",
    foreground=colors[10],
    font="FiraMono Nerd Font",
    fontsize=20,
    padding=8,
    decorations=_left_decor(colors[6]),
)

w_volume = widget.PulseVolume(
    foreground=colors[6],
    limit_max_volume="True",
    # mouse_callbacks={"Button3": open_pavu},
    padding=8,
    decorations=_right_decor(colors[6]),
)

# internet
w_wlan = (
    (
        widget.Wlan(
            format="直",
            foreground=colors[10],
            disconnected_message="睊",
            fontsize=16,
            interface=wifi,
            update_interval=5,
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(home + "/.local/bin/nmgui"),
            },
            padding=4,
            decorations=_left_decor(colors[2]),
        ),
        separator_sm(),
        widget.Wlan(
            format="{percent:2.0%}",
            disconnected_message=" ",
            interface=wifi,
            update_interval=5,
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(home + "/.local/bin/nmgui"),
            },
            padding=8,
            decorations=_right_decor(colors[2]),
        ),
        separator(),
    )
    if wifi is not None
    else ()
)

# time, calendar
def gen_clock():
    color = colors[8]
    return (
        widget.TextBox(
            text="",
            font="FiraMono Nerd Font",
            fontsize=16,
            foreground=colors[10],  # blue
            padding=8,
            decorations=_left_decor(color),
            #mouse_callbacks={"Button1": open_calendar},
        ),
        separator_sm(),
        widget.Clock(
            format="%b %d, %H:%M",
            foreground=color,
            padding=8,
            decorations=_right_decor(color),
            #mouse_callbacks={"Button1": open_calendar},
        ),
        separator(),
    )


# power menu
w_power = widget.TextBox(
    text="⏻",
    background=colors[0],
    foreground="#000000",
    font="FiraMono Nerd Font",
    fontsize=18,
    padding=16,
    mouse_callbacks={"Button1": open_powermenu},
)

w_test = widget.WidgetBox(
    close_button_location="right",
    fontsize=24,
    font="FiraMono Nerd Font",
    text_open=" ",
    text_closed=" ",
    widgets=[w_systray],
    decorations=_left_decor(colors[2]),
)
