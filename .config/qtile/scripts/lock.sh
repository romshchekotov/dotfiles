#!bash

# COLORS
date_color="#DDB6F2"
ring_color="#1E1E2E"
wrong_color="#F28FAD"
verify_color="#DDB6F2"
ring_out_color="#ABE9B3"
highlight_color="#B5E8E0"
unlocker_color="#00000000"
foreground_color="#D9E0EE"

# TEXT
verifying_text="Verifying Password..."
wrong_text="Wrong Password!"
greeter_text="Type the password to unlock"
text_font="FiraMono Nerd Font"
text_size=23

# POSITIONING
time_pos="300:500"
date_pos="300:630"
ind_pos="1620:520"
greeter_pos="1620:630"
wrong_pos="1620:670"
verif_pos="1620:670"

# IMAGES
lock_screen_img="$HOME/.config/qtile/wallpapers/bicycle-cityscape.jpg"

# UTIL
dim_re="s/^[^0-9]*([0-9]+x[0-9]+).*$/\1/"

# CMD
i3lock-color -i "$lock_screen_img" -F \
  -n --force-clock \
  -e --indicator \
  --radius=40 \
  --ring-width=8 \
  --inside-color=$unlocker_color \
  --ring-color=$foreground_color \
  --insidever-color=$verify_color \
  --ringver-color=$verify_color \
  --insidewrong-color=$wrong_color \
  --ringwrong-color=$wrong_color \
  --line-uses-inside \
  --keyhl-color=$ring_color \
  --separator-color=$verify_color \
  --bshl-color=$ring_color \
  --time-str="%H:%M" \
  --time-size=140 \
  --date-str="%a, %d %b" \
  --date-size=45 \
  --verif-text=$verifying_text \
  --wrong-text=$wrong_text \
  --noinput-text="" \
  --greeter-text=$greeter_text \
  --time-font=$text_font \
  --date-font=$text_font \
  --verif-font=$text_font \
  --greeter-font=$text_font \
  --wrong-font=$text_font \
  --verif-size=$text_size \
  --greeter-size=$text_size \
  --wrong-size=$text_size \
  --time-pos=$time_pos \
  --date-pos=$date_pos \
  --ind-pos=$ind_pos \
  --greeter-pos=$greeter_pos \
  --wrong-pos=$wrong_pos \
  --verif-pos=$verif_pos \
  --date-color=$date_color \
  --time-color=$date_color \
  --greeter-color=$foreground_color \
  --wrong-color=$wrong_color \
  --verif-color=$verify_color \
  --pointer=default \
  --refresh-rate=0 \
  --pass-media-keys \
  --pass-volume-keys
