#!bash

set -euo pipefail

mute () {
 muted=$(pulsemixer --get-mute)
 if [[ "$muted" == "0" ]]; then
   echo "" > /tmp/vol-icon
 else 
   echo "婢" > /tmp/vol-icon
 fi
}

if [ -p /tmp/vol ] && [ -p /tmp/vol-icon ]; then
  true
else
  mkfifo /tmp/vol && pulsemixer --get-volume | awk '{print $1}' > /tmp/vol 
  mkfifo /tmp/vol-icon && mute
fi

script_name="temp_vol.sh"
for pid in $(pgrep -f $script_name); do
  if [ "$pid" != "$$" ]; then
    kill -9 "$pid"
  fi
done

vol_inc=5
case $1 in
  up)
    currentVolume=$(pulsemixer --get-volume | awk '{print $1}')
    if [[ "$currentVolume" -ge "100" ]]; then
      pulsemixer --max-volume 100
    else
      pulsemixer --change-volume +"$vol_inc"
    fi ;;
  down)
    pulsemixer --change-volume -"$vol_inc" ;;
  mute)
    muted=$(pulsemixer --get-mute)
    if [[ "$muted" == "0" ]]; then
      pulsemixer --toggle-mute
      echo "婢" > /tmp/vol-icon
    else 
      pulsemixer --toggle-mute
      echo "" > /tmp/vol-icon
    fi ;;
esac

pulsemixer --get-volume | awk '{print $1}' > /tmp/vol 
