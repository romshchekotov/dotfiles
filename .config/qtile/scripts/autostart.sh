#!bash

picom -b &
eww daemon &
volctl &
mkfifo /tmp/vol-icon && ~/.config/qtile/scripts/volume_icon.sh &
