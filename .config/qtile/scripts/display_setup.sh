#!bash

num=$(xrandr --listmonitors | head -n 1 | cut -d" " -f 2)

xrandr --output eDP-1 --mode 2736x1824 --rate 59.96
if [[ num -eq 2 ]]; then
  xrandr --output DP-1 \
    --mode 3840x2160 \
    --rate 60.00 \
    --right-of eDP-1
elif [[ num -eq 3 ]]; then
  # TODO: Never happened yet :sweat:
fi
