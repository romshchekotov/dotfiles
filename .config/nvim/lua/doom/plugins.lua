return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'

	use 'preservim/nerdtree'
	use 'tpope/vim-fugitive'
	use 'ctrlpvim/ctrlp.vim'

  use 'SirVer/ultisnips'
  use 'honza/vim-snippets'
	use 'dense-analysis/ale'
	use 'Shougo/deoplete.nvim'

	use { 'Fymyte/rasi.vim', ft = 'rasi', requires = { 'ap/vim-css-color' } }
  use 'lervag/vimtex'
	use 'LnL7/vim-nix'

	use 'NLKNguyen/papercolor-theme'
	use 'joshdick/onedark.vim'
	use 'vim-airline/vim-airline'
	use 'vim-airline/vim-airline-themes'
	use 'ryanoasis/vim-devicons'
	use 'mhinz/vim-startify'

end)
