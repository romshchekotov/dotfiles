function setNighttime()
  vim.opt.background = "dark"
  vim.cmd('colorscheme onedark')
  vim.g['airline_theme'] = 'onedark'
end

function setDaytime()
  vim.opt.background = "light"
  vim.cmd('colorscheme PaperColor')
  vim.g['airline_theme'] = 'papercolor'
end

vim.api.nvim_create_user_command(
  'Nighttime', setNighttime,
  {desc = 'Set the Nighttime Theme'}
)

vim.api.nvim_create_user_command(
  'Daytime', setDaytime,
  {desc = 'Set the Daytime Theme'}
)
