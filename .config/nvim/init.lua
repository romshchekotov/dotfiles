require('doom.plugins')
require('doom.keymap')
require('doom.funcx')

local set = vim.opt

set.number = true
set.tabstop = 2
set.shiftwidth = 2
set.softtabstop = 2
set.expandtab = true

set.hlsearch = true
set.incsearch = true
set.ignorecase = true
set.smartcase = true

vim.cmd('syntax enable')
set.list = true
set.listchars = { tab = '▸ ', trail = '·' }
set.termguicolors = true

vim.g['airline#extensions#tabline#enabled'] = 1
vim.g['airline#extensions#ale#enabled'] = 1
vim.g['airline#extensions#tabline#formatter'] = 'unique_tail'
vim.g['airline_powerline_fonts'] = 1

local hour = os.date("*t").hour
if (hour > 7 and hour < 20) then
	setDaytime()
else
	setNighttime()
end

vim.g['vimtex_view_method'] = 'zathura'
vim.g['vimtex_compiler_method'] = 'latexmk'

vim.g['UltiSnipsExpandTrigger'] = "<tab>"
vim.g['UltiSnipsJumpForwardTrigger'] = "<c-j>"
vim.g['UltiSnipsJumpBackwardTrigger'] = "<c-k>"
vim.g['UltiSnipsEditSplit'] = "vertical"

vim.g['ale_fixers'] = {
	'c',
	'css',
	'dockerfile',
	'gitcommit',
	'terraform'
}
vim.g['ale_fixers']['c'] = {"clang-format","clangd","cppcheck","gcc"}
vim.g['ale_fixers']['css'] = {"stylelint"}
vim.g['ale_fixers']['dockerfile'] = {"hadolint"}
vim.g['ale_fixers']['gitcommit'] = {"gitlint"}
vim.g['ale_fixers']['terraform'] = {"terraform-fmt"}

vim.g['ale_sign_error'] = ''
vim.g['ale_sign_warning'] = ''

vim.g['deoplete#enable_at_startup'] = 1
vim.cmd("call deoplete#custom#option('sources', { '_': ['ale'] })")
