;;; README.org --- Doomer's Kitty Terminal Configuration

;;; Commentary:
;; 

#+TITLE: Kitty Configuration
#+AUTHOR: Roman Shchekotov
#+auto_tangle: t

* Configuration
** Fonts
#+BEGIN_SRC conf :tangle kitty.conf
font_family FiraCode Nerd Font Mono
font_size   12.0
#+END_SRC

** Cursor
#+BEGIN_SRC conf :tangle kitty.conf
cursor_shape               underline
cursor_underline_thickness 1.0
#+END_SRC

** Scroll-Back
#+BEGIN_SRC conf :tangle kitty.conf
scrollback_lines        2000
wheel_scroll_multiplier 3.0
touch_scroll_multiplier 1.0
#+END_SRC

** Mouse
#+BEGIN_SRC conf :tangle kitty.conf
mouse_hide_wait 3.0

detect_urls     yes
url_color       #61AFEF
url_style       straight
url_prefixes    file ftp ftps git http https irc mailto sftp ssh

copy_on_select  yes
#+END_SRC

*** Mouse Bindings
#+BEGIN_SRC conf :tangle kitty.conf
mouse_map middle release ungrabbed paste_from_selection
mouse_map left press ungrabbed mouse_selection normal
mouse_map ctrl+left press ungrabbed mouse_selection rectangle
mouse_map left doublepress ungrabbed mouse_selection word
mouse_map left triplepress ungrabbed mouse_selection line
#+END_SRC

** Performance
#+BEGIN_SRC conf :tangle kitty.conf
sync_to_monitor yes
repaint_delay   10
input_delay     3
#+END_SRC

** Window Layout
*TODO:* set-up the =window_logo_path ./logo.png=
and the related options: 
- =window_logo_position bottom-right=
- window_logo_alpha 0.5

#+BEGIN_SRC conf :tangle kitty.conf
remember_window_size       yes
initial_window_width       640
initial_window_height      400
window_border_width        0.5pt
window_margin_width        1
single_window_margin_width -1
window_padding_width       1
active_border_color        #61AFEF
inactive_border_color      #5C6370
inactive_text_alpha        0.7

enabled_layouts            Stack,Fat
hide_window_decorations    no
confirm_os_window_close    0
#+END_SRC

** Tabs
*TODO:* work on the [[https://sw.kovidgoyal.net/kitty/conf/#opt-kitty.tab_title_template][=tab_title_template=]].
#+BEGIN_SRC conf :tangle kitty.conf
tab_bar_edge        bottom
tar_bar_style       powerline
tab_bar_align       center
tab_bar_min_tabs    2
tab_powerline_style angled
tab_activity_symbol ⚗️

active_tab_foreground   #ABB2BF
active_tab_background   #282C34
active_tab_font_style   bold-italic
inactive_tab_foreground #5C6370
inactive_tab_background #282C34
inactive_tab_font_style normal
#+END_SRC

** Color Scheme
Mostly the OneDark scheme.
#+BEGIN_SRC conf :tangle kitty.conf
background_opacity 0.8

foreground #abb2bf
background #282c34

# selection_foreground #000000
# selection_background #fffacd

#: Black
color0 #282c34
color8 #3e4452

#: Red
color1 #e06c75
color9 #e06c75

#: Green
color2  #98c379
color10 #98c379

#: Yellow
color3  #e5c07b
color11 #e5c07b

#: Blue
color4  #61afef
color12 #61afef

#: Purple
color5  #c678dd
color13 #c678dd

#: Cyan
color6  #56b6c2
color14 #56b6c2

#: White
color7  #5c6370
color15 #abb2bf
#+END_SRC

** Applications
#+BEGIN_SRC conf :tangle kitty.conf
shell zsh --login
editor nvim
#+END_SRC

** Advanced & Miscellaneous
#+BEGIN_SRC conf :tangle kitty.conf
close_on_child_death  yes
update_check_interval 0
#+END_SRC
Allows up-to 64 Megabytes of Memory when storing contents to the clipboard
and sets up permissions to read and write from the clipboard.
#+BEGIN_SRC conf :tangle kitty.conf
clipboard_control  write-clipboard write-primary read-clipboard-ask read-primary-ask
clipboard_max_size 64
#+END_SRC
#+BEGIN_SRC conf :tangle kitty.conf
allow_hyperlinks  yes
shell_integration enabled
term              xterm-kitty

linux_display_server auto
#+END_SRC

** Keybindings
#+BEGIN_SRC conf :tangle kitty.conf
map ctrl+shift+c       copy_to_clipboard
map ctrl+shift+v       paste_from_clipboard

map f11 toggle_fullscreen
map f10 toggle_maximized

map cmd+enter       new_window
map shift+cmd+d     close_window
map cmd+]           next_window
map cmd+[           previous_window

map ctrl+t       new_tab
map ctrl+w        close_tab
map ctrl+tab        next_tab
map ctrl+shift+tab previous_tab

map ctrl+alt+l next_layout

map ctrl+= change_font_size all +2.0
map ctrl+- change_font_size all -2.0
map ctrl+0 change_font_size all 0
map ctrl+f1 change_font_size all 12.0
map ctrl+f2 change_font_size all 24.0

map ctrl+u    kitten unicode_input
map ctrl+cmd+space kitten unicode_input

map ctrl+a>j set_background_opacity +0.1
map ctrl+a>k set_background_opacity -0.1

map ctrl+f5 load_config_file
map ctrl+k clear_terminal to_cursor active
#+END_SRC

*** Special Selection Gimmicks
Set-Up Multikey shortcuts for following functionality, in it's order:
- Open Selected Path
- Insert Selected Hash
- Open the selected file at the selected line
#+BEGIN_SRC conf :tangle kitty.conf
map ctrl+p>shift+f kitten hints --type path
map ctrl+p>h kitten hints --type hash --program -
map ctrl+p>n kitten hints --type linenum
#+END_SRC

(provide 'README)

;;; README.org ends here
