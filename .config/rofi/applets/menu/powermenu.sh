#!bash

# Thanks Aditya :D
dir="$HOME/.config/rofi/applets/menu/configs/rounded"
rofi_command="rofi -theme $dir/powermenu.rasi"

uptime=$(uptime -p | sed -e 's/up //g')
cpu=$($HOME/.config/rofi/bin/usedcpu)
memory=$($HOME/.config/rofi/bin/usedram)

# Options
shutdown=""
reboot=""
lock=""
suspend="鈴"
logout=""

# Themes
style_dir="$HOME/.config/rofi/applets/styles"
confirm_theme="$style_dir/confirm.rasi"
message_theme="$style_dir/message.rasi"

# Confirmation
confirm_exit() {
  rofi -dmenu \
    -i \
    -no-fixed-num-lines \
    -p "Are You Sure? : " \
    -theme "$confirm_theme"
}

# Message
msg() {
  rofi -theme "$message_theme" -e "Available Options  -  yes / y / no / n"
}

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$suspend\n$logout"

chosen="$(echo -e "$options" | $rofi_command -p "祥  $uptime  |     $cpu  |  ﬙  $memory " -dmenu -selected-row 2)"
case $chosen in
  $shutdown)
    ans=$(confirm_exit &)
    if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
      systemctl poweroff
    elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
      exit 0
    else
      msg
    fi
    ;;
  $reboot)
    ans=$(confirm_exit &)
    if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
      systemctl reboot
    elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
      exit 0
    else
      msg
    fi
    ;;
  $lock)
    bash ~/.config/qtile/scripts/lock.sh
    ;;
  $suspend)
    ans=$(confirm_exit &)
    if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
      mpc -q pause
      amixer set Master mute
      systemctl suspend
    elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
      exit 0
    else
      msg
    fi
    ;;
  $logout)
    ans=$(confirm_exit &)
    if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
      if [[ "$DESKTOP_SESSION" == "Openbox" ]]; then
        openbox --exit
      elif [[ "$DESKTOP_SESSION" == "bspwm" ]]; then
        bspc quit
      elif [[ "$DESKTOP_SESSION" == "i3" ]]; then
        i3-msg exit
      elif [[ "$DESKTOP_SESSION" == *"qtile"* ]]; then
        qtile cmd-obj -o cmd -f shutdown
      fi
    elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
      exit 0
    else
      msg
    fi
    ;;
esac
